/**
 * 
 */
package com.example.dto;

/**
 * @author victo
 *
 */
public class User {
	
	private String name;
	private String health;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the health
	 */
	public String getHealth() {
		return health;
	}
	/**
	 * @param health the health to set
	 */
	public void setHealth(String health) {
		this.health = health;
	}
	
	@Override
	public String toString() {
		return "{name : " +  getName() + ", health: " + getHealth() +  "}";
	}

}
