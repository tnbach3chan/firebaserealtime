/**
 * 
 */
package com.example.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.stereotype.Service;

import com.example.dto.User;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * @author victo
 *
 */

@Service
public class FirebaseService {

	public static final String COL_NAME = "users";
//	public static final String COL_NAME = "items";

	public String saveUserToCloudFirestore(User user) throws InterruptedException, ExecutionException {
		Firestore dbFirestore = FirestoreClient.getFirestore();
		ApiFuture<WriteResult> collectionApiFuture = dbFirestore.collection(COL_NAME).document(user.getName())
				.set(user);

		return collectionApiFuture.get().getUpdateTime().toString();
	}

	public User getUserDetailsFromCloudFirestore(String name) throws InterruptedException, ExecutionException {
		Firestore dbFirestore = FirestoreClient.getFirestore();
		DocumentReference documentReference = dbFirestore.collection(COL_NAME).document(name);
		ApiFuture<DocumentSnapshot> future = documentReference.get();

		DocumentSnapshot document = future.get();

		User user = null;

		if (document.exists()) {
			user = document.toObject(User.class);
			return user;
		} else
			return null;
	}

	public String saveUserToRealtimeDB(User user) {
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference ref = database.getReference("chat");
//		DatabaseReference vnpostRef = ref.child("chat");
		ref.push().setValueAsync(user);
		return "success";
	}

	public List<User> getUserDetailsFromRealtimeDB() {
		
		final AtomicInteger count = new AtomicInteger();
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference ref = database.getReference("/chat");
		List<User> users = new ArrayList<>();

		ref.addChildEventListener(new ChildEventListener() {

			@Override
			public void onChildRemoved(DataSnapshot snapshot) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onChildMoved(DataSnapshot snapshot, String previousChildName) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onChildChanged(DataSnapshot snapshot, String previousChildName) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onChildAdded(DataSnapshot snapshot, String previousChildName) {
//				int newCount = count.incrementAndGet();
				User user = snapshot.getValue(User.class);
				users.add(user);
				System.out.println("user:" + user);
//				System.out.println("Added " + snapshot.getKey() + ", count is " + newCount);
			}

			@Override
			public void onCancelled(DatabaseError error) {
				// TODO Auto-generated method stub

			}
		});

//		ref.addListenerForSingleValueEvent(new ValueEventListener() {
//			  @Override
//			  public void onDataChange(DataSnapshot snapshot) {
//			    long numChildren = snapshot.getChildrenCount();
//			    System.out.println(count.get() + " == " + numChildren);
//			  }
//
//			  @Override
//			  public void onCancelled(DatabaseError databaseError) {}
//			});
		
		System.out.println("List users: " + Arrays.toString(users.toArray()));
		return users;
	}
	
}
