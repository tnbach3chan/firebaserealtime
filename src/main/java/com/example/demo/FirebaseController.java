/**
 * 
 */
package com.example.demo;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.User;

/**
 * @author victo
 *
 */

@RestController
public class FirebaseController {
	
	@Autowired
	private FirebaseService firebaseService;
	
	@PostMapping("/createU")
	public String postUser(@RequestBody User user) throws InterruptedException, ExecutionException {
		return firebaseService.saveUserToCloudFirestore(user);
	}
	
	@GetMapping("/getU")
	public User getUser(@RequestParam String name) throws InterruptedException, ExecutionException {
		return firebaseService.getUserDetailsFromCloudFirestore(name);
	}
	
	@PostMapping("/createUser")
	public String createUser(@RequestBody User user) {
		return firebaseService.saveUserToRealtimeDB(user);
	}
	
	@GetMapping("/getUser")
	public List<User> getUser() {
		return firebaseService.getUserDetailsFromRealtimeDB();
	}

	@PostMapping("/saveUser")
	public String saveUser(@RequestBody User user) {
		return firebaseService.saveUserToRealtimeDB(user);
	}
}
