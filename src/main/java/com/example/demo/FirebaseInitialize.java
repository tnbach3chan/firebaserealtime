/**
 * 
 */
package com.example.demo;

import java.io.FileInputStream;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;


/**
 * @author victo
 *
 */
@Component
public class FirebaseInitialize {

	@PostConstruct
	public void initialize() throws IOException {

		FileInputStream serviceAccount = new FileInputStream("./serviceAccountKey.json");

		FirebaseOptions options = new FirebaseOptions.Builder()
				.setCredentials(GoogleCredentials.fromStream(serviceAccount))
				.setDatabaseUrl("https://vnpost-test.firebaseio.com").build();

		FirebaseApp.initializeApp(options);

//		DatabaseReference ref = FirebaseDatabase.getInstance().getReference("/some_resource");
//		ref.addListenerForSingleValueEvent(new ValueEventListener() {
//			@Override
//			public void onDataChange(DataSnapshot snapshot) {
//				Object document = snapshot.getValue();
//				System.out.println(document);
//			}
//			
//			@Override
//			public void onCancelled(DatabaseError error) {
//				
//			}
//		});

	}
}
